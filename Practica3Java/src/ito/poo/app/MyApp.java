package ito.poo.app;
import java.time.LocalDate;
import ito.CuentaBancaria;

public class MyApp {

	public static void main(String[] args) {
		
		CuentaBancaria c1=new CuentaBancaria(93029, "Ximena Trujillo", 2000f, LocalDate.of(2021, 1, 4));
		CuentaBancaria c2=new CuentaBancaria(33950, "Jessica Hernández", 700f, LocalDate.of(2020, 9, 28));
		System.out.println(c1);
	    System.out.println(c2); 
	     
	    System.out.println(!c1.equals(c2));
	    System.out.println(c2.compareTo(c1));
	}
}
